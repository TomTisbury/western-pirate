﻿using System;

namespace WesternPirate
{
    class Levels
    {
        //Bedroom
        public static void Level1()
        {
            string A;
            Console.Clear();
            Console.WriteLine("It's early morning and you wake up as your eyes start to ajust to the light you see a light switch");
        Part1:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "switch on the light":
                    Console.WriteLine("As you switch the light on you can see your phone, your uniform, a bag and your earphones");
                    goto Part2;

                case "turn on the light":
                    Console.WriteLine("As you turn on the light you can see your phone, your uniform, a bag and your earphones");
                    goto Part2;

                default:
                    goto Part1;

            }
        Part2:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "look at the phone":
                    Console.WriteLine("As you look at your phone you see the time 7:{0} what I need to get up", Program.Varibles.MorningTime);
                    goto Part3;

                case "turn on the phone":
                    Console.WriteLine("As you turn on your phone you see the time 7:{0} what I need to get up", Program.Varibles.MorningTime);
                    goto Part3;

                default:
                    goto Part2;
            }
        Part3:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "get up":
                    Console.WriteLine("As you get up you trip over your uniform");
                    varibles.rush = false;
                    goto Part4;

                case "stay in bed":
                    Console.WriteLine("You stay in bed and fall back to sleep");
                    goto part5;

                default:
                    goto Part3;
            }
        Part4:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "change into your uniform":
                    Console.WriteLine("You get changed into your uniform you pick up your phone, earphones and bag you then walk down stairs to the kitchen");
                    Inventory.Found.Phone = true;
                    Inventory.Found.Bag = true;
                    Inventory.Found.Headphones = true;
                    goto part6;

                case "get into the uniform":
                    Console.WriteLine("You get into your uniform you pick up your phone, earphones and bag you then walk down stairs to the kitchen");
                    goto part6;

                default:
                    goto Part4;
            }

        part5:
            Console.WriteLine("Wake Up, what the heck as you slowly wake up you relise the time is 8:{0} what no the bus leaves school at 8:{1}", Program.Varibles.MorningTime, Program.Varibles.MorningTime + 10);
        part5a:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "get up":
                    Console.WriteLine("You quickly get up changing into your uniform and picking up your bag");
                    varibles.rush = true;
                    goto part7;

                default:
                    goto part5a;
            }

        part6:
            Console.WriteLine("What would you like to eat for breakfast");
        part6a:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "cereal":
                    Console.WriteLine("Here you go some corn flakes");
                    goto part7;

                case "toast":
                    Console.WriteLine("Ok I will put some toast in the toaster");
                    int gb = Program.Varibles.rnd.Next(0, 1);
                    switch (gb)
                    {
                        case 0:
                            Console.WriteLine("Here you go some toast");
                            varibles.rush = false;
                            break;

                        case 1:
                            Console.WriteLine("As you get your toast you are on your phone looking at facebook and then go to pick up the toast but miss and hit a glass of water great now I have to clean that up");
                            varibles.rush = true;
                            break;
                    }
                    goto part7;

                default:
                    Console.WriteLine("Sorry I dont have any of that in");
                    goto part6a;

            }

        part7:
            Console.WriteLine("You walk out the door and get in the car and you drive to school");
        part7a:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "hurry up":
                    if (varibles.rush)
                    {
                        Console.WriteLine("Well it is your fault that we left late");
                        Level2();
                    }
                    else
                    {
                        Console.WriteLine("It is ok we have plenty of time");
                        Level2();
                    }
                    break;

                case "Oh good plenty of time":
                    if (varibles.rush)
                    {
                        Console.WriteLine("No we don't have much time");
                        Level2();
                    }
                    else
                    {
                        Console.WriteLine("Yes we should be there in plenty of time if the traffic is good");
                        Level2();
                    }
                    break;

                default:
                    goto part7a;
            }

        }

        //School
        public static void Level2()
        {
            string A;
            if (varibles.rush)
            {
                int yn = Program.Varibles.rnd.Next(0, 1);
                switch (yn)
                {
                    case 0:
                        Console.WriteLine("You just make it to school before the bus leaves, you get on the bus and sit next to your freind steve who has saved you a seat");
                        goto part2;

                    case 1:
                        Console.WriteLine("As you arrive at school you see the bus leaving oh great now I have to drive you to your trip");
                        A = Console.ReadLine().ToLower();
                        switch (A)
                        {
                            case "sorry":
                                Console.WriteLine("It's alright it isn't to far");
                                break;

                            case "well maybe you should have driven faster":
                                if (Inventory.Found.Phone)
                                {
                                    Console.WriteLine("Well it was your fault that we left late becasue of that phone so give me it");
                                    Inventory.Found.Phone = false;
                                    Console.WriteLine("You might as well have my headphones as well");
                                    Inventory.Found.Headphones = false;
                                    goto part2;
                                }
                                else
                                {
                                    Console.WriteLine("Well maybe if you didn't get up so late we wouldn't be late rather than me driving faster");
                                    goto part2;
                                }
                        }
                        break;
                }
            }
            else
            {
                Console.WriteLine("You arrive at school with plenty of time and sit next to your freind steve");
                goto part2;
            }

        part2:
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "hi steve how are you today":
                    Console.WriteLine("Hi I am fine thanks you");
                    A = Console.ReadLine().ToLower();
                    switch (A)
                    {
                        case "fine":
                            Console.WriteLine("That is good are you looking forward to today");
                            A = Console.ReadLine().ToLower();
                            switch (A)
                            {
                                case "yeah":
                                    Console.WriteLine("Yeah I am too");
                                    goto part3;

                                case "not really":
                                    Console.WriteLine("Oh I am");
                                    goto part3;
                            }
                            break;
                    }
                    break;

                case "hi how are you today":
                    Console.WriteLine("I am ok a bit tired though you");
                    A = Console.ReadLine().ToLower();
                    switch (A)
                    {
                        case "fine":
                            Console.WriteLine("That is good are you looking forward to today");
                            A = Console.ReadLine().ToLower();
                            switch (A)
                            {
                                case "yeah":
                                    Console.WriteLine("Yeah I am too");
                                    goto part3;

                                case "not really":
                                    Console.WriteLine("Oh I am");
                                    goto part3;
                            }
                            break;
                    }
                    break;
            }

        part3:
            A = Console.ReadLine().ToLower();
            Console.WriteLine("As you are driving to the trip you get bored and play a game of red car with steve which you win you then arrive at your trip the local Natural Museum");
            Console.WriteLine("as you walk in you see that the place is massive and realise how big it really is when you start to walk around you then come to the western section and find it more intresting than the saxons so you");
            Console.WriteLine("persuade steve to come with you and you both leave the group amd go and look at the section more");
            Console.WriteLine("You see a sign one to the interactive section and one to the information section which one do you goto interactive or information");
            A = Console.ReadLine().ToLower();
            switch (A)
            {
                case "interactive":
                    Console.WriteLine("You decide to go to the interactive section");
                    break;

                case "information":
                    Console.WriteLine("You decide to go to the information section");                                                                                                                                                                                                                                                                           
                    break;
            }

        }

        public class varibles
        {
            public static bool rush;
        }
    }
}
