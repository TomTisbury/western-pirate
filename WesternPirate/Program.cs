﻿using System;
using System.Media;
using AudioSwitcher.AudioApi.CoreAudio;
using CSCore;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace WesternPirate
{
    class Program
    {
        //Main Thread
        static void Main(string[] args)
        {
            GameLoad();
        }
        
        //Game Load
        static void GameLoad()
        {
            Console.Title = "Western Pirate";
            Varibles.MainMenuMusic.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + @"\MainMenu Music.wav";
            Varibles.GameMusic.SoundLocation = AppDomain.CurrentDomain.BaseDirectory + @"\Game Music.wav";
            PreMenu();
        }

        //Varibles
        public static class Varibles
        {
            public static Random rnd = new Random();
            public static string SaveName = "",CurrentLevel, Warning = "", ProgramName = System.AppDomain.CurrentDomain.FriendlyName;
            public static int CurrentLevelNumber, MorningTime = rnd.Next(1, 30), Volume = 100;
            public static List<string> SavesAvalible;
            public static ConsoleKey KeySelected;
            public static SoundPlayer MainMenuMusic = new SoundPlayer(), GameMusic = new SoundPlayer();
            public static CoreAudioDevice defaultPlaybackDevice = new CoreAudioController().DefaultPlaybackDevice;
        }

        //Pre Menu
        static void PreMenu()
        {
            Varibles.MainMenuMusic.Play();
            Menu();
        }

        //Main Menu
        static void Menu()
        {
            Console.Clear();
            string[] Horse = Properties.Settings.Default.Horse.Split("1"[0]);
            foreach(string H in Horse)
            {
                Console.WriteLine(H);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("█   █ █▀▀ █▀▀ ▀▀█▀▀ █▀▀ █▀▀█ █▀▀▄ 　  █▀▀█ ▀█▀  █▀▀█ █▀▀█ ▀▀█▀▀ █▀▀");
            Console.WriteLine("█ █ █ █▀▀ ▀▀█   █   █▀▀ █▄▄▀ █  █ 　  █▄▄█  █   █▄▄▀ █▄▄█   █   █▀▀");
            Console.WriteLine("█▄▀▄█ ▀▀▀ ▀▀▀   ▀   ▀▀▀ ▀ ▀▀ ▀  ▀ 　  █    ▀▀▀  ▀ ▀▀ ▀  ▀   ▀   ▀▀▀");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine("Welcome to Western Pirate what would you like to do");
            Console.WriteLine("Press 1 to start a new game");
            Console.WriteLine("Press 2 to load a save game");
            Console.WriteLine("Press 3 for more information about the game and help");
            Console.WriteLine("Press 4 for options");
            Console.WriteLine("Press 5 or esc to exit the game");
            Varibles.KeySelected = Console.ReadKey().Key;
            switch (Varibles.KeySelected)
            {
                case ConsoleKey.D1:
                    New();
                    break;

                case ConsoleKey.D2:
                    LoadSaveChooser();
                    break;

                case ConsoleKey.D3:
                    HAI();
                    break;

                case ConsoleKey.D4:
                    Options();
                    break;

                case ConsoleKey.D5:
                    Varibles.MainMenuMusic.Stop();
                    Environment.Exit(0);
                    break;

                case ConsoleKey.NumPad1:
                    New();
                    break;

                case ConsoleKey.Escape:
                    Varibles.MainMenuMusic.Stop();
                    Environment.Exit(0);
                    break;

                case ConsoleKey.NumPad2:
                    LoadSaveChooser();
                    break;

                case ConsoleKey.NumPad3:
                    HAI();
                    break;

                case ConsoleKey.NumPad4:
                    Options();
                    break;

                case ConsoleKey.NumPad5:
                    Varibles.MainMenuMusic.Stop();
                    Environment.Exit(0);
                    break;

                case ConsoleKey.T:
                    Console.WriteLine("Enter save test name");
                    Varibles.SaveName = Console.ReadLine();
                    Save();
                    break;

                default:
                    Varibles.MainMenuMusic.Stop();
                    Menu();
                    break;
            }
        }

        //Options
        static void Options()
        {
            Console.Clear();
            Console.WriteLine("Please enter how loud you would like the music to be and press enter or");
            Console.WriteLine("type esc to exit back to the menu");
            Console.WriteLine("Current Volume:" + Varibles.defaultPlaybackDevice.Volume);
            Varibles.KeySelected = Console.ReadKey().Key;
            switch (Varibles.KeySelected)
            {
                case ConsoleKey.OemPlus:
                    Varibles.defaultPlaybackDevice.Volume = Varibles.defaultPlaybackDevice.Volume + 5;
                        break;

                case ConsoleKey.OemMinus:
                    Varibles.defaultPlaybackDevice.Volume = Varibles.defaultPlaybackDevice.Volume - 5;
                    break;

                case ConsoleKey.Escape:
                    Menu();
                    break;
            }
            Options();
        }

        //Game Saving
        static void Save()
        {
            File.AppendAllText(Varibles.SaveName + ".sav",Varibles.CurrentLevel + Environment.NewLine);
        }

        //Load Save Choose
        static void LoadSaveChooser()
        {
            int Count = 1;
            foreach (string F in Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory))
            {
                if (F.Contains(".sav"))
                {
                    Console.WriteLine(Count + F.Replace(".sav", ""));
                    Varibles.SavesAvalible.Add(F);
                    Count++;
                }
            }
            Console.WriteLine("Please enter the number of the the save you wish to load or press esc to go back to the menu");
            Varibles.KeySelected = Console.ReadKey().Key;
            switch (Varibles.KeySelected)
            {
                case ConsoleKey.D1:
                    Varibles.SaveName = Varibles.SavesAvalible[0];
                    break;
                case ConsoleKey.D2:
                    Varibles.SaveName = Varibles.SavesAvalible[1];
                    break;
                case ConsoleKey.D3:
                    Varibles.SaveName = Varibles.SavesAvalible[2];
                    break;
                case ConsoleKey.D4:
                    Varibles.SaveName = Varibles.SavesAvalible[3];
                    break;
                case ConsoleKey.D5:
                    Varibles.SaveName = Varibles.SavesAvalible[4];
                    break;
                case ConsoleKey.D6:
                    Varibles.SaveName = Varibles.SavesAvalible[5];
                    break;
                case ConsoleKey.D7:
                    Varibles.SaveName = Varibles.SavesAvalible[6];
                    break;
                case ConsoleKey.D8:
                    Varibles.SaveName = Varibles.SavesAvalible[7];
                    break;
                case ConsoleKey.D9:
                    Varibles.SaveName = Varibles.SavesAvalible[8];
                    break;
                case ConsoleKey.NumPad1:
                    Varibles.SaveName = Varibles.SavesAvalible[0];
                    break;
                case ConsoleKey.NumPad2:
                    Varibles.SaveName = Varibles.SavesAvalible[1];
                    break;
                case ConsoleKey.NumPad3:
                    Varibles.SaveName = Varibles.SavesAvalible[2];
                    break;
                case ConsoleKey.NumPad4:
                    Varibles.SaveName = Varibles.SavesAvalible[3];
                    break;
                case ConsoleKey.NumPad5:
                    Varibles.SaveName = Varibles.SavesAvalible[4];
                    break;
                case ConsoleKey.NumPad6:
                    Varibles.SaveName = Varibles.SavesAvalible[5];
                    break;
                case ConsoleKey.NumPad7:
                    Varibles.SaveName = Varibles.SavesAvalible[6];
                    break;
                case ConsoleKey.NumPad8:
                    Varibles.SaveName = Varibles.SavesAvalible[7];
                    break;
                case ConsoleKey.NumPad9:
                    Varibles.SaveName = Varibles.SavesAvalible[8];
                    break;
                case ConsoleKey.Escape:
                    Menu();
                    break;
            }
            LoadSave();
        }

        //Load Save
        static void LoadSave()
        {
            Varibles.MainMenuMusic.Stop();
            string[] Save = File.ReadAllLines(Varibles.SaveName + ".sav");
            Varibles.CurrentLevel = Save[0];
            Rooms();
        }

        //New Game
        static void New()
        {
            Varibles.MainMenuMusic.Stop();
            Console.Clear();
            Varibles.CurrentLevel = "Level1";
            Varibles.CurrentLevelNumber = 1;
            Console.WriteLine("Please enter a name for the save");
            Varibles.SaveName = Console.ReadLine();
            Console.WriteLine("Thank You Please Press Any Key To Begin...");
            Console.ReadKey();
            Rooms();
        }

        //Help and Information
        static void HAI()
        {
            Console.Clear();
            Console.WriteLine("Made by Thomas Tisbury");
            Console.WriteLine("To play the game simply enter a action to perform to continue the story and progress");
            Console.WriteLine("Music Used: Main Menu - Game Music -");
            Console.WriteLine("Press esc to go back to the menu");
            Varibles.KeySelected = Console.ReadKey().Key;
            switch (Varibles.KeySelected)
            {
               case ConsoleKey.Escape:
                    Menu();
                    break;

                default:
                    HAI();
                    break;
            }
        }

        //Rooms
        static void Rooms()
        {
            Varibles.GameMusic.Play();
            switch (Varibles.CurrentLevel)
            {
                case "Level1":
                    Levels.Level1();
                    break;

                case "Level2":
                    Levels.Level2();
                    break;
            }
        }
        }
    }